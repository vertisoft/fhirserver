package ca.uhn.fhir.jpa.demo;

import javax.net.ssl.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.uhn.fhir.model.api.Bundle;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import ca.uhn.fhir.rest.method.RequestDetails;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;

import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by LarsKristian on 30.10.2015.
 */
public class RequestInterceptor extends InterceptorAdapter {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RequestInterceptor.class);

    @Override
    public boolean incomingRequestPreProcessed(HttpServletRequest theRequest, HttpServletResponse theResponse) {

        String authorization = theRequest.getHeader("Authorization");
        logger.info("incomingRequestPreProcessed Authorization header:"+authorization);

        if (authorization == null) {
            theResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            theResponse.setHeader("AuthServer","https://fhirauth.azurewebsites.net/connect/authorize");
            logger.info("No Authorization header.  ");
            return false;
        }

        String authToken = authorization.replace("Bearer","").trim();

        if (authToken.contains("REJECT")) {
            theResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        } else if (!checkAuthentication(authToken)) {
            logger.info("Not accepting auth token... ");
            theResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        } else {
            return true;
        }
        /*
        https://fhirauth.azurewebsites.net/connect/accesstokenvalidation?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSIsImtpZCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSJ9.eyJjbGllbnRfaWQiOiJpbXBsaWNpdGNsaWVudCIsInNjb3BlIjoid3JpdGUiLCJzdWIiOiI4MTg3MjciLCJhbXIiOiJwYXNzd29yZCIsImF1dGhfdGltZSI6MTQ0NjI0Mjg3NiwiaWRwIjoiaWRzcnYiLCJpc3MiOiJodHRwczovL2ZoaXJhdXRoLmF6dXJld2Vic2l0ZXMubmV0IiwiYXVkIjoiaHR0cHM6Ly9maGlyYXV0aC5henVyZXdlYnNpdGVzLm5ldC9yZXNvdXJjZXMiLCJleHAiOjE0NDYyNDY0NzksIm5iZiI6MTQ0NjI0Mjg3OX0.WJ-_ARmjz2QTOYZR728QzQkaI1hPUHeEujTwov-S0-_kz2Cl0NLcwUOVo1hWePDsTRm72OmWBnixd4wBILHHOBRJGS53gAeXOzYm0ck9u-Sc9ggvaLzUOIgENu1SRR0DmjF2gS5HJOCugiIzdx-ncaqOh1C5zGugca7_LY82DOexwjCuhwKlMHqeoE26xuGU6VHf3umCMMAzaftO1XYZuPteu9jDK1qYbsGNox71pLQcextbsMKBqbNt6tbejIgIcu6Q15BLJT06Lscb8TCMyhbkjIt96bqd2shmA6uUuOCBSVAzSzfsxVm3Ocrog-QoDru9pM4T2pyB3h4yHukmQg
        --->>>
             {
                "client_id": "implicitclient",
                "scope": "write",
                "sub": "818727",
                "amr": "password",
                "auth_time": "1446242876",
                "idp": "idsrv",
                "iss": "https://fhirauth.azurewebsites.net",
                "aud": "https://fhirauth.azurewebsites.net/resources",
                "exp": "1446246479",
                "nbf": "1446242879"
            }
         */

    }

    @Override
    public void incomingRequestPreHandled(RestOperationTypeEnum theOperation, IServerInterceptor.ActionRequestDetails theRequestDetails) {

        logger.info("incomingRequestPreHandled code:"+theOperation.getCode());
        logger.info("incomingRequestPreHandled resource type:"+theRequestDetails.getResourceType());

        // TODO: Convert any incoming data here to the correct details "de-anonymize it"
    }

    @Override
    public boolean outgoingResponse(RequestDetails theRequestDetails,
                                    Bundle theResponseObject,
                                    HttpServletRequest theServletRequest,
                                    HttpServletResponse theServletResponse) {
        logger.info("outgoingResponse called");
        return true;

    }

    // Todo: The below is a hack... it was late at night... This should be using resttemplate or something nicer...
    // It also shouldn't accept any certificate. That's awful actually!
    boolean checkAuthentication(String token) {

        try {
            try {

                final String url = "https://fhirauth.azurewebsites.net/connect/accesstokenvalidation?token="+token;
                boolean success = false;

                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }};
                // Install the all-trusting trust manager
                final SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname,
                                          SSLSession session) {
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                HttpsURLConnection c = (HttpsURLConnection) new URL(url)
                        .openConnection();
                c.setUseCaches(false);
                c.connect();

                success = readStream(c.getInputStream());
                c.disconnect();
                return success;

            } catch (Exception e) {
                logger.info("Exception:" + e);
                return false;
            }


        } catch (Exception e) {
            logger.error("checkAuthentication:" + e);
            return false;
        }
    }

    private boolean readStream(InputStream in) {
        BufferedReader reader = null;
        boolean success = false;
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                logger.info(">"+line);
                if (line.contains("scope")) {
                    logger.info("Accepting it");
                    success = true;
                }
            }
        } catch (IOException e) {
            logger.debug("Exception:" + e);
            success = false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    logger.debug("Exception:" + e);
                    success = false;
                }
            }
        }
        return success;
    }

    /*
    boolean checkAuthentication(String token) {

        try {

            final String uri = "https://fhirauth.azurewebsites.net/connect/accesstokenvalidation";
            Map<String, String> params = new HashMap<String, String>();
            params.put("token", token);

            RestTemplate restTemplate = new RestTemplate();
            String result = restTemplate.getForObject(uri, String.class, params);

            logger.info("checkAuthentication"+result);

            return true;

        } catch(Exception e) {
            logger.error("checkAuthentication:"+e);
            return false;
        }
    }
    */

    /*
    boolean checkAuthentication(String token) {

        try {

            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet("https://fhirauth.azurewebsites.net/connect/accesstokenvalidation?token="+token);
            HttpResponse response = client.execute(request);
            logger.info("Checking token response:" + response.getStatusLine().getStatusCode());
            logger.info("checkAuthentication"+response.getProtocolVersion());
            logger.info("checkAuthentication"+response.getStatusLine().getStatusCode());
            logger.info("checkAuthentication"+response.getStatusLine().getReasonPhrase());
            logger.info("checkAuthentication"+response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                long len = entity.getContentLength();
                if (len != -1 && len < 2048) {
                    logger.info("checkAuthentication"+EntityUtils.toString(entity));
                } else {
                    // Stream content out
                }
            }

            return true;

        } catch(Exception e) {
            logger.error("checkAuthentication:"+e);
            return false;
        }
        */
    }

