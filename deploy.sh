#!/bin/bash

service tomcat7 stop
rm -rf /var/lib/tomcat7/webapps/ROOT*
cp target/ROOT.war /var/lib/tomcat7/webapps/
service tomcat7 start
tail -f /var/lib/tomcat7/logs/catalina.out

